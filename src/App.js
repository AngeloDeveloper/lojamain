import logo from './logo.svg';
import './App.css';
import Routes from './routes'
import './scss/style.scss';
function App() {
  return (
    <Routes></Routes>
  );
}

export default App;
