import React from 'react'
import {BrowserRouter,Switch, Link, Redirect,Route} from 'react-router-dom'
import Dashboard from "./pages/dashboard";
import TheSidebar from "./pages/dashboard/sidebar";

import Login from './pages/telalogin/telalogin'
import ProjetoFirebase from "./firebaseservices";
import TheLayout from './pages/dashboard/TheLayout';
var autenticacaoPadrao = ProjetoFirebase.auth();

function PrivateRoute ({component: Component, authed, ...rest}) {
    return (
      <Route
        {...rest}
        render={(props) => authed === true
          ? <Component {...props} />
          : <Redirect to={{pathname: '/login', state: {from: props.location}}} />}
      />
    )
  }
  
  function PublicRoute ({component: Component, authed, ...rest}) {
    return (
      <Route
        {...rest}
        render={(props) => authed === false
          ? <Component {...props} />
          : <Redirect to='/dashboard' />}
      />
    )
  }
  

class Routes extends React.Component{
    state = {
        authed: false,
        loading: true,
      }

      componentDidMount () {
        this.removeListener = autenticacaoPadrao.onAuthStateChanged((user) => {
          if (user) {
            this.setState({
              authed: true,
              loading: false,
            })
          } else {
            this.setState({
              authed: false,
              loading: false
            })
          }
        })
      }
      componentWillUnmount () {
    this.removeListener()
  }






    render(){   
            return this.state.loading === true ? <h1>Loading</h1> : (
                <BrowserRouter>
                    <Switch>
                      {/*  <Route path="/login" component={Login} ></Route>
                        <Route path="/dashboard" component={Dashboard} ></Route> */}
                        <PublicRoute authed={this.state.authed} path='/login' component={Login}/>
                        <PrivateRoute authed={this.state.authed} path='/' component={TheLayout} />
                        <Route render={() => <h3>No Match</h3>} />
                    </Switch>
                </BrowserRouter>
            );
    }
}
export default Routes;
