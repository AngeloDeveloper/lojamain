import produtobase from './foodb.svg'
import remove from './remove.svg'
import list from './list.svg'


let styles = {
  marginRight: '20px',
};
let styles2 = {
  marginRight: '10px',
};

const produtoicon= <img src={produtobase} alt="Logo" width="24" height="24"  style={styles} />;
const removeicon= <img src={remove} alt="Logo" width="20" height="20"  style={styles2} />;
const listicon= <img src={list} alt="Logo" width="24" height="24"  style={styles2} />;


export {
    produtoicon,
    removeicon,
    listicon
  }