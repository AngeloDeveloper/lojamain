import React from 'react';
//import Teste from "./teste"

const CriarProduto = React.lazy(() => import('./conteudos/CriarProduto'));
const ProdutosLista = React.lazy(()=> import ("./conteudos/produtosLista"))
const EditarProduto = React.lazy(()=> import ('./conteudos/EditarProduto'))



const routes = [
  { path: '/produtos/criar', name: 'CriarProduto', component: CriarProduto },
  { path: '/produtos/lista', name: 'CriarProduto', component: ProdutosLista },
  { path: '/produtos/editar/:id', name: 'CriarProduto', component: EditarProduto },
];

export default routes;
