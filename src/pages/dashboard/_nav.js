import React from 'react'
import CIcon from '@coreui/icons-react'
import { listicon, produtoicon} from "../../static/icons"

const _nav =  [
  {
    _tag: 'CSidebarNavItem',
    name: 'Dashboard',
    to: '/dashboard',
    icon: <CIcon name="cil-speedometer" customClasses="c-sidebar-nav-icon"/>,
    badge: {
      color: 'info',
      text: 'NEW',
    }
  },
   //-------- isso é um componente com titulo e filho------//
  {
    _tag: 'CSidebarNavTitle',
    _children: ['EXEMPLO DE TITULO']
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'EXEMPLO DE FILHO',
    to: '/theme/colors',
    icon: produtoicon,
  },
  //-------- ----------------------------------------------//

    
  //------------isso é um exemplo de titulo com filhos e dropdown--------//
  {
    _tag: 'CSidebarNavTitle',
    _children: ['Social']
  },
  {
    _tag: 'CSidebarNavDropdown',
    name: 'TITULO FILHO',
    route: '/base',
    icon: 'cil-puzzle',
    _children: [
      {
        _tag: 'CSidebarNavItem',
        name: 'Exemplo de subtitulo',
        to: '/base/breadcrumbs',
      },
    ],
  },
  {
    _tag: 'CSidebarNavDropdown',
    name: 'Usuários',
    route: '/base',
    icon: produtoicon,
    _children: [
      {
        _tag: 'CSidebarNavItem',
        name: 'Criar novo usuário',
        to: '/usuarios/criar',
      },
    ],
  },
    //------------------------------------------------------------------------//
  //------------isso é um exemplo de titulo com filhos e dropdown--------//
  {
    _tag: 'CSidebarNavTitle',
    _children: ['Produto']
  },
  {
    _tag: 'CSidebarNavDropdown',
    name: 'Criar produto',
    route: '/base',
    icon: produtoicon,
    _children: [
      {
        _tag: 'CSidebarNavItem',
        name: 'Criar produto simples',
        to: '/produtos/criar',
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Criar produto personalizado',
        to: '/base/breadcrumbs',
      },
       
    ],
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Listar produtos',
    to: '/produtos/lista',
    icon: listicon,
  },
    //------------------------------------------------------------------------//
]


export default _nav
