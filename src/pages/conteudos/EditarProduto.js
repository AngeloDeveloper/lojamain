import React from 'react'
import Adicionais from "./adicionais"
import ProjetoFirebase from "../../firebaseservices";
import {
  CButton,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CForm,
  CFormGroup,
  CFormText,
  CTextarea,
  CInput,
  CInputFile,
  CLabel,
  CSelect,
  CRow,
  CInvalidFeedback,
  CSwitch,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { useParams } from 'react-router';
const {useState, useEffect} = React;

const categorias = ProjetoFirebase.firestore().collection('Categorias');
const produtos = ProjetoFirebase.firestore().collection('Produtos');

const EditarProduto = () => {
  const { id } = useParams();
  console.log(id);
  const ProdutoRef = ProjetoFirebase.firestore().collection('Produtos').doc(id);
 
  var DefaltValues;

  async function getDocument() {
    // [START get_document]
    // [START firestore_data_get_as_map]
    const doc = await ProdutoRef.get();
    if (!doc.exists) {
      return null;
    } else {
      //return doc.data();
     SetCamposNomeProd(doc.data().Nome)
     SetPrecoProd(doc.data().Preco)
     setElements(doc.data().Adicionais)
     SetDescricao(doc.data().Descricao)
    }
    // [END firestore_data_get_as_map]
    // [END get_document]
  }
 


  const [elements,setElements] = useState([]);
  const [categoriaAtiva,setCategoriaAtiva]=useState('Todas');
  const [ObservacaoCliente,AllowObservacao] = useState(true);
  const [ProdutoAtivado,AllowProduto] = useState(true);
  const [ParaMaiores,AllowMaiores] = useState(false);
  const [descricao,SetDescricao]= useState("");


  // ---------------------------------------------AREA DA CATEGORIA----------------------------------------
  const [categoriaValida,SetCatValida]=useState(false)
  const [categoria, setCategoria] = useState('')
  const [todasCategorias, setTodas] = useState([])
  const [errosCategoria, setErrosCategoria] = useState('')
  
   function validaCategoria(value) {
   
    if (!categoria.replace(/\s/g, '').length) {
      setErrosCategoria("O nome não pode ser vazio")
      SetCatValida(true);
      return;
    }

    if(categoria===""){
      setErrosCategoria("O nome não pode está nulo")
      SetCatValida(true);
      return;
    }
    if(todasCategorias.indexOf(value) > -1){
      console.log("entrei aqui")
      console.log("mesm q o value de categoria seja")
      console.log(categoria)
      setErrosCategoria("já existe uma classe com esse nome")
      SetCatValida(true);
      return;
    }  
      SetCatValida(false);
      setErrosCategoria("")
  }

  async function cadastraCategoria() {
     
    // [START firestore_data_query]
    //const snapshot = await categorias.where('nome', '==', categoria).get();
    if (!categoriaValida) {
      categorias.add({
        "nome": categoria
      }).then((docRef) => {
        alert("Categoria criada com sucesso")
      })
      .catch((error) => {
        alert("erro ao criar categoria")
        alert("Error adding document: ", error);
      });
    }else{
      //alert("já existe uma categoria com esse nome")
    }
    
  }
  function selectCategoria(event) {
    setCategoriaAtiva(event.target.value);
    retornaCategorias();
  }


   async function retornaCategorias() {
     
    // [START firestore_data_query]
    const snapshot = await categorias.where('nome', '!=', null).get();
    var vetorDeCategorias = []
    if (!snapshot.empty) {
        snapshot.forEach(doc => {
        vetorDeCategorias.push(doc.data().nome);
      });
    }
    snapshot.forEach(doc => {
      setTodas(vetorDeCategorias);
    });
  }
  useEffect(() => {
    getDocument()
   retornaCategorias();
  
  }, [])
  
  function setCategoriaf(event) {
     setCategoria(event.target.value);  
     validaCategoria(event.target.value);
  }
//-------------------------------------------------FIM DAS CATEGORIAS---------------------------------

//-----------------------------------------------CAMPOS DO PRODUTO-----------------------------------------
const [camposValidos,SetCamposValidos]= useState(false)
//-------------------------------------------------NOME DO PRODUTO------------------------------------
const [nomeValido,SetNomeValido] = useState(true)
const [nomeDoProduto,SetCamposNomeProd]= useState('')

function validaNome(event) {
   

  if (event.target.value.length===1) {
      SetNomeValido(true);
      return
  }

  if (!nomeDoProduto.replace(/\s/g, '').length || event.target.value==="" ) {
      setErrosCategoria("O nome não pode ser vazio");
      SetNomeValido(false);
      SetCamposValidos(false);
      return;
  }
    SetCamposValidos(true);
    SetNomeValido(true);
    //console.log("passou not este")
}

function setNomeProd(event) {
  SetCamposNomeProd(event.target.value);  
  validaNome(event);
}

//-------------------------------------------------PRECO DO PRODUTO--------------------------------------------
const [precoValido,SetPrecoValido] = useState(true)
const [precoDoProduto,SetPrecoProd]= useState('')

function validaPreco(event) {
   
  const re = /^[0-9\,\b]+$/;
  //console.log("?")
  //console.log(re.test(event.target.value))
  // if value is not blank, then test the regex

  if (!re.test(event.target.value)) {
    SetPrecoValido(false);
    SetCamposValidos(false);
    //console.log(re.test(event.target.value))
    return
  }else if (event.target.value.length===1) {
      SetPrecoValido(true);
      return
  }

  if (!precoDoProduto.replace(/\s/g, '').length || event.target.value==="" ) {
      //setErrosCategoria("O preço não pode ser vazio");
      SetPrecoValido(false);
      SetCamposValidos(false);
      return;
  }
    SetCamposValidos(true);
    SetPrecoValido(true);
    //console.log("passou not este")
}

function SetPrecoProdf(event) {
  SetPrecoProd(event.target.value);  
  validaPreco(event);
}

function CadastraProduto() {
  if(camposValidos){
    ProdutoRef.set({
      "Nome": nomeDoProduto,
      "Preco": precoDoProduto,
      "Categoria":categoriaAtiva,
      "Descricao":descricao,
      "Adicionais":elements,
      "Ativado": ProdutoAtivado,
      "Obs Cliente": ObservacaoCliente,
      "Para maiores": ParaMaiores,

    }).then((docRef) => {
      alert("Produto criado com sucesso")
    })
    .catch((error) => {
      alert("erro ao criar categoria")
      alert("Error adding document: ", error);
    });

  }else{
    alert("Por favor verifique os campos")
  }


}


//-----------------------------------------------FIM   DO PRODUTO-----------------------------------------


//-----------------------------------------------PRODUTOS ADICIONAIS----------------------------------------
const [nome,setNome]=useState('')
const [preco,setPreco]=useState('')

const [nomeADValido,SetNomeADValido] = useState(false)

function validaNomeAdicional(event) {
   
  if (event.target.value.length===1) {
      SetNomeADValido(true);
      return
  }

  if (!nome.replace(/\s/g, '').length || event.target.value==="" ) {
      //setErrosCategoria("O nome não pode ser vazio");
      SetNomeADValido(false);
      //SetCamposValidos(false);
      return;
  }
    //SetCamposValidos(true);
    SetNomeADValido(true);
    //console.log("passou not este")
}

function setNomeProdAD(event) {
  setNome(event.target.value);  
  validaNomeAdicional(event);
}

const [precoADValido,SetPrecoADValido] = useState(false)


function validaPrecoAD(event) {
   
  const re = /^[0-9\,\b]+$/;
  //console.log("?")
  //console.log(re.test(event.target.value))
  // if value is not blank, then test the regex

  if (!re.test(event.target.value)) {
    SetPrecoADValido(false);
    //console.log(re.test(event.target.value))
    return
  }else if (event.target.value.length===1) {
      SetPrecoADValido(true);
      return
  }

  if (!preco.replace(/\s/g, '').length || event.target.value==="" ) {
      console.log("sou eu q to baguncando")
      SetPrecoADValido(false);
      return;
  }
     
    SetPrecoADValido(true);
    //console.log("passou not este")
}

function SetPrecoADValidof(event) {
  setPreco(event.target.value);  
  validaPrecoAD(event);
}

//--------------------------------------------FIM  PRODUTOS ADICIONAIS----------------------------------------

  function removeElement(event){
        
      var filteredItems = elements.filter(function (item) {
        return (item.nome !== event.target.value);
      });

      setElements(filteredItems);
};
  
  function addEntryClick() {
    if(nomeADValido && precoADValido ){
      setElements(elements => [...elements, { 'nome': nome, 'preco': preco }]);
    }else{
      alert("por favor verifique todos os campos")
    }
    
  }
  
  return (
    
    <>
      <CCol md="6000">
      <CCard>
            <CCardHeader>
              Criar categoria
              <small> </small>
            </CCardHeader>
            <CCardBody>
              <CForm action="" method="post" inline>
                <CFormGroup className="pr-1">
                  <CLabel htmlFor="exampleInputName2" className="pr-1">Nome</CLabel>
                  <CInput onClick={validaCategoria} invalid={categoriaValida} onChange={setCategoriaf}  id="exampleInputName2" placeholder="Exemplo: bebidas" required />
                  <CInvalidFeedback>{errosCategoria}</CInvalidFeedback>
                </CFormGroup>
                
                
                <CFormGroup className="pr-1">
                  <CButton onClick={cadastraCategoria} size="sm" color="success">Adicionar categoria</CButton>
                </CFormGroup>
              </CForm>
            </CCardBody>
             
          </CCard>



          <CCard>
            <CCardHeader>
              Especifique as caracteristicas do produto
              <small></small>
            </CCardHeader>
            <CCardBody>
              <CForm action="" method="post" encType="multipart/form-data" className="form-horizontal">
              <CFormGroup row>
                  <CCol md="3">
                    <CLabel htmlFor="select">Categoria</CLabel>
                  </CCol>
                  <CCol xs="12" md="9">
                  
                    <CSelect size="6" onClick={selectCategoria} custom name="select" id="select">
                    
                      <React.Fragment>
                      {todasCategorias.map((cadatupla, i) => {  return (
                         <option value={cadatupla}>{cadatupla} </option>
                      );})
                      }
                                             
                       </React.Fragment>
                  
                    </CSelect>
                  </CCol>
                </CFormGroup>
                <CFormGroup row>
                  <CCol md="3">
                    <CLabel htmlFor="text-input">Nome do produto</CLabel>
                  </CCol>
                  <CCol xs="12" md="9">
                    <CInput value={nomeDoProduto} invalid={!nomeValido} onChange={setNomeProd} onClick={validaNome} id="text-input" name="text-input" placeholder="Insira o nome aqui" />
                    <CFormText>This is a help text</CFormText>
                  </CCol>
                  <CCol md="3">
                    <CLabel htmlFor="text-input">Preço do produto</CLabel>
                  </CCol>
                  <CCol xs="12" md="9">
                    <CInput value={precoDoProduto} invalid={!precoValido} onChange={SetPrecoProdf} onClick={validaPreco} id="text-input" name="text-input" placeholder="Insira o preço aqui" />
                    <CFormText>Por favor apenas números separados por virgula</CFormText>
                  </CCol>
                  
                </CFormGroup>
                <CFormGroup row>
                  <CCol md="3">
                    <CLabel htmlFor="textarea-input">Descrição(opcional)</CLabel>

                    <CFormGroup row>
                  <CCol tag="label" sm="9" className="col-form-label">
                   Observações do cliente
                  </CCol>
                  <CCol sm="3">
                    <CSwitch
                      onChange={(event)=>AllowObservacao(event.target.checked)}
                      className="mr-1"
                      color="dark"
                      defaultChecked
                      shape="pill"
                      variant="opposite"
                    />
                  </CCol>
                 
                  <CCol tag="label" sm="9" className="col-form-label">
                    Produto ativado
                  </CCol>
                  <CCol sm="3">
                    <CSwitch
                      onChange={(event)=>AllowProduto(event.target.checked)}
                      className="mr-1"
                      color="dark"
                      defaultChecked
                      shape="pill"
                      variant="opposite"
                    />
                  </CCol>
                  
                  <CCol tag="label" sm="9" className="col-form-label">
                    Para maiores de 18 anos
                  </CCol>
                  <CCol sm="3">
                    <CSwitch
                      className="mr-1"
                      onChange={(event)=>AllowMaiores(event.target.checked)}
                      color="dark"
                      shape="pill"
                      variant="opposite"
                    />
                  </CCol>
                </CFormGroup>

                  </CCol>
                  <CCol xs="12" md="9">
                    <CTextarea 
                      value={descricao}
                      onChange={(event)=>SetDescricao(event.target.value)}
                      name="textarea-input" 
                      id="textarea-input" 
                      rows="9"
                      placeholder="Exemplo... O melhor sorvete do Brasil agora com a cobertura de chocolate" 
                    />
                    
                  </CCol>
                  {/*PARTE DA ADICAO*/}
                </CFormGroup>
                      <CFormGroup row>
                        <CCol md="3">
                          <CLabel>Nome do adicional</CLabel>
                        </CCol>
                        <CCol xs="12" md="9">
                          <p className="form-control-static">Preço por unidade</p>
                        </CCol>
                        
                      </CFormGroup>
                      <CFormGroup col>
                     {/*   elements*/}
                     <Adicionais elementos={elements} delete={removeElement} ></Adicionais>
                        </CFormGroup>
                <CCardBody>
                  <CForm action="" method="post" inline>
                    
                    <CFormGroup className="pr-1">
                      <CInput invalid={!nomeADValido} onChange={setNomeProdAD} size="sm" id="InputName4" placeholder="Exemplo: batata frita" required />
                      <CInput invalid={!precoADValido} onChange={SetPrecoADValidof} size="sm" id="InputName4" placeholder="Exemplo: 29,99" required />
                    </CFormGroup>
                    <CFormGroup className="pr-1">
                      <CButton size="sm" color="success" onClick={addEntryClick}>Adicionar opcional</CButton>
                    </CFormGroup>
                  </CForm>
                </CCardBody>
                
              
                <CFormGroup row>
                  <CLabel col md={3}>Custom file input</CLabel>
                  <CCol xs="12" md="9">
                    <CInputFile custom id="custom-file-input"/>
                    <CLabel htmlFor="custom-file-input" variant="custom-file">
                      Choose file...
                    </CLabel>
                  </CCol>
                </CFormGroup>
              </CForm>
            </CCardBody>
            <CCardFooter>
              <CButton onClick={CadastraProduto} type="submit" size="sm" color="primary"><CIcon name="cil-scrubber" /> Submit</CButton>
              <CButton type="reset" size="sm" color="danger"><CIcon name="cil-ban" /> Reset</CButton>
            </CCardFooter>
          </CCard>
         
        </CCol>
     
      
    </>
  )
}

export default EditarProduto
