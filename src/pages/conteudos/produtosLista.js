import React, { useState, useCallback } from 'react'
import ProjetoFirebase from "../../firebaseservices";
import { useHistory } from "react-router-dom";

import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow
} from '@coreui/react'
//import { DocsLink } from 'src/reusable'

const {useEffect} = React;

const fields = ['Nome','Preco', 'Categoria', 'Descricao',]

function ProdutosLista() {
    const history = useHistory();
    const handleOnClick = useCallback((event) => history.push('/produtos/editar/'+(event.ID)), [history]);

    const produtosdb = ProjetoFirebase.firestore().collection('Produtos');
    const [Produtos,SetProdutos]= useState('');
    async function retornaProdutos() {
        
        const snapshot = await produtosdb.get();
        var vetorDeProdutos = []
        if (!snapshot.empty) {
            snapshot.forEach(doc => {
            vetorDeProdutos.push({...doc.data(),"ID":doc.id});
        });
        }
        snapshot.forEach(doc => {
        SetProdutos(vetorDeProdutos);
        });
        //console.log(Produtos);
    }

    useEffect(() => {

        retornaProdutos();
       
    }, [])












    return (
        <>
           
            <CRow>
                <CCol>
                    <CCard>
                        <CCardHeader>
                                                Combined All Table
                        </CCardHeader>
                        <CCardBody>
                            <CDataTable
                                items={Produtos}
                                fields={fields}
                                hover
                                striped
                                bordered
                                size="sm"
                                itemsPerPage={10}
                                pagination
                                onRowClick={handleOnClick}
                                 />
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
            
        </>
    )
}

export default ProdutosLista
