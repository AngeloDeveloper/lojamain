import React from 'react'
 

import {
  CButton,
  CCol,
  CLabel,
  CRow,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
class Adicionais extends React.Component {   

    render(){
        return( 
            <React.Fragment>
           {this.props.elementos.map((cadatupla, i) => {         
               return (<React.Fragment>
                <CRow>
                <CCol md="3">
                  <CLabel>{cadatupla.nome}</CLabel>
                </CCol>
                <CCol>
                <CRow md="712">
                  <p className="form-control-static">{cadatupla.preco}  R$                           </p>
                </CRow>
                </CCol>
                <CCol  md="3">
                <CButton onClick={this.props.delete} type="reset" size="sm" color="danger" value={cadatupla.nome}> Remover</CButton>
                </CCol>
                </CRow>
                </React.Fragment>);
                 
            })}</React.Fragment>)
    }
}   
export default Adicionais;